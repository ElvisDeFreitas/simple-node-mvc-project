var request = require("request"),
		util = require("util");

module.exports.controller = function(app) {
	
	app.get("/", function(req, res){
		res.render("index", {
			compile: false,
			debug: process.debug
		});
	});

	app.get("/logs", app.is("this.isAdmin()"), function(req, res){
		res.writeHead(200, {
		 "Content-Type" : "application/json", "Access-Control-Allow-Origin": "*" 
		});
		var rl = require('readline').createInterface({
		  input: require('fs').createReadStream(__dirname + "/../../logs/log.log")
		});
		rl.on('line', function (line) {
		  res.write(line);
		  res.write('\n');
		});
		rl.on('close', function (line) {
		  res.end();
		});
	});

};