var mysql = require('mysql'),
		sync = require('synchronize');

module.exports = function(config){
	var pool = mysql.createPool(config);

	// criando query sincronizada
	pool.queryAsync = pool.query;
	sync(pool, 'getConnection');

	/** 
	 * roda a query com a tabela locada
	 * tables ex: 'T1 WRITE, T2 WRITE ....'
	 */
	pool.queryLock= function(tables, bulk, callback, callback2){
		var con;
		if(typeof bulk == "string"){
			sync.fiber(function(){
				try{
					con = pool.getConnection();
					sync(con, 'query');
					con.query("SET autocommit=0;LOCK TABLES " + tables);
					var r = con.query(bulk, callback);
					con.query("commit;SET autocommit=1;UNLOCK TABLES;");
					con.release();
					return r;
				}catch(e){
					if(con){
						con.rollback();
						con.query("UNLOCK TABLES;");
						con.release();
					}
					throw e;
				}
			}, callback2);
		}else{
			sync.fiber(function(){
				try {
					con = pool.getConnection();
					con.query("SET autocommit=0;LOCK TABLES " + tables);
					sync(con, 'query');
					var r = bulk.call(con);
					con.query("commit;SET autocommit=1;UNLOCK TABLES;");
					con.release();
					return r;
				}catch(e){
					if(con){
						con.rollback();
						con.query("UNLOCK TABLES;");
						con.release();
					}	
					throw e;
				}
			}, function(err){if(err)callback(err);});
		}
	}

	/**
	 * Executa uma função que tem uma lista de queries de forma sincrona
	 */
	pool.queryBulk = function(bulk, callback){
		var con;
		sync.fiber(function() {
			try{
				con = pool.getConnection();
				con.query('SET autocommit=0;');
				sync(con, 'query');
				bulk.call(con);
				con.query('commit;SET autocommit=1;');
				con.release();
			}catch(e){
				if(con){
					con.release();
					con.rollback();
				}
				throw e;
			}
		}, function(err){if(err)callback(err);});

	};

	/**
	 * Executa a query e fecha a conexão automaticamente
	 */
	pool.queryAuto = function(query, params, callback){
		var con;
		sync.fiber(function() {
			try{
				con = pool.getConnection();
				sync(con, 'query');
				var r = con.query.call(con, query, params);
				con.release();
				return r;
			}catch(e){
				if(con)con.release();
				throw e;
			}
		}, callback);

	};

	return pool;
}

