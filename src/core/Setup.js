var	mysql = require("./Mysql"),
		fs = require('fs'),
		express = require('express'),
    app = {};

module.exports = function(disable){
	disable = disable || [];

	return trigger({

		/**
		 * suporte para funcções que ainda vão existir ou para funções que não existem mais na core do node
		 */
		'setupCoreFunctions': function(){
			require('./CoreFunctions')();
		},
		/**
		 * set as configurações essenciais para o resto do sistema
		 */
		'setupMain': function(){
			app = express();
	 		var cookieParser = require("cookie-parser"),
			    session = require("cookie-session"),
			    path = require('path'),
			    bodyParser = require('body-parser');

			// some environment variables
			app.set('port', process.env.PORT || 3000);

			// express configuration
			app.use(cookieParser());
			app.use(bodyParser.urlencoded({ extended: true }));
			app.use(session({
			  secret: 'keyboard cat',
			  resave: true,
			  saveUninitialized: false 
			}));
			app.use(bodyParser.json());
			app.use(express.static(path.join(__dirname, 'public')));
			app.locals.appName = "Simple node MVC Project";

		},

		'setupUtils': function(){
			app.getModel = process.getModel = function(file){
				return require('../model/' + file);
			}
		},

		/**
		 * Configura a ferramenta de view
		 */
		'setupLayout': function(){
			var mustacheLayout = require('mustache-layout');
			mustacheLayout.debug(false);
			app.set('views', './view');
			app.set('view engine', 'html');
			app.set("view options", {layout: true});
			app.engine("html", mustacheLayout);
		},

		/**
		 * Configura a ferramenta que trata as mensagens de exceções para o cliente
		 */
		'setupErrorResponseManager': function(){
			app.em = process.em = require("./ErrorManager")(app);
		},

		/**
		 * configura as variáveis do app no process e seta o modo do sistema
		 */
		'setupSystemMode': function(){
			process.app = app;
			app.debug = process.debug = process.env.ND_DEBUG === 'debug' || process.env.ND_DEBUG === 'test'
		},

		/**
		 * Configura o logger
		 */
		'setupLogger': function(){
			var logger;
			if(process.env.ND_DEBUG === 'test'){
				logger = console;
			}else{
				logger = require('./Logger')(app);
			}
	    process.c = app.c = logger;
			app.log = process.log = function(){
				app.c.info.apply(app.c, arguments);
			}
		},

		/**
		 * configura o programa de internacionalização
		 */
		'setupSubtitle': function(){
				process.subtitle = require("./l18n")(process.cwd() + '/l18n');
		 },

		 /** 
		  * configura o sistema em caso de falha crítica
		  */ 
		'setupUncaught': function(){
			var that = this;
			process.on('uncaughtException', function (error) {
				try{
				  process.c.error("The system has crashed on async: %s", error.stack);
				  app.db.close();
				  if(error.code == 'SQLITE_ERROR'){
				  	try{
							that.setupConnection();
						}catch(e){
							process.c.error("Conexão não pode ser reaberta, sistema será fechado", e.stack);
							process.exit(-1);
						}
				  }
				}catch(e){}
			});
	  },

	  /**
	   * configura o autenticador do sistema
	   */
	  'setupAuthentication': function(){
			require("./Authentication")(app);
	  },

	  /**
		 * configura o pool de conexão
		 */
	  'setupConnection': function(){
	  	process.db = app.db = mysql({
				poolLimit: 30,
			  host: '127.0.0.1',
			  user: 'root',
			  password: 'root',
			  database: 'mageddo_api',
			  multipleStatements: true
			});
	  },

	  /**
	   * configura o motor de regras que verifica se o usuário está logado
	   * e se o usuário atende á regra passada
	   */
	  'setupMotorRegras': function(){
	  	var motor = require('./MotorRegras')({
	  		isAdmin: function(){
	  			return this.admin;
	  		}
	  	});
	  	app.is = process.is = function(rule){
	  		return function(req, res, next){
		  		if(req.user){
		  			var authenticated = motor.verify(req.user.rolesMap, rule);
		  			if(authenticated){
		  				next();
		  			}else{
		  				console.info("dados: ", req.user);
		  				res.send('Logado mas Sem permissão, você não é: ' + rule, 403);
		  			}
		  		}else{
		  			res.send('Não logado', 403);
		  		}
	  		};
	  	};
	  }
	});

	function trigger(triggers){
		Object.keys(triggers).map(function(fName){
			if(disable.indexOf(fName) === -1){
				console.log('carregando: ', fName);
				triggers[fName].call(triggers);
			}
		});
		return app;
	};
};
