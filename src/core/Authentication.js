var passport = require('passport');
var Strategy = require('passport-local').Strategy;

module.exports = function(app){
	
	var m = app.getModel('UserModel')();

	// Strategy.prototype.authenticate = function(req, options) {
	// 	console.log("autenticando.....", this);
	// 	this.success({
	// 		id: 1,
	// 		name: 'elvis'
	// 	})

	// };

	passport.use(new Strategy(
  function(username, password, cb) {
    m.getUserByEmail(username, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (user.password != password) { return cb(null, false); }
      return cb(null, user);
    });
  }));


	passport.serializeUser(function(user, cb) {
		console.log("guardando o user...");
	  cb(null, user.email);
	});

	passport.deserializeUser(function(email, cb) {
		console.log("pegando o user...", email);
	  m.getUserByEmail(email, function (err, user) {
	    if (err) { return cb(err); }
	    cb(null, user);
	  });
	});

	app.use(passport.initialize());
	app.use(passport.session());


	app.get('/login',
	  function(req, res){
	    res.render('login');
	  });
	  
	app.post('/login', 
	  passport.authenticate('local', { failureRedirect: '/login' }),
	  function(req, res) {
	  	req.sessionOptions.maxAge = 1000 * 60 * 60 * 10;
	    res.redirect('/');
	  });
	  
	app.get('/logout',
	  function(req, res){
	    req.logout();
	    res.redirect('/');
	  });

}