module.exports = function(defaultFunctions){
	defaultFunctions = defaultFunctions || {};

	function verify(regra){
	  return eval(regra) === true;
	}
	return {
		verify: function(roles, rule){
			return verify.call(Object.assign({}, defaultFunctions, roles), rule)
		}
	};

}