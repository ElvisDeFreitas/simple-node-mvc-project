module.exports = function(app){
	return {
		_500: function(options){
			return treat(app, options, 500, 'error');
		},
		_400: function(options){
			return treat(app, options, 400, 'info');
		}

	}
};
function treat(app, options, code, logger){

	// internacionalizando
	var subl;
	if(!options.req){
		subl = process.subtitle.getSubtitle('en');
	}else{
		subl = process.subtitle.getSubtitleByReq(options.req);
	}
	if(subl[options.message]){
		options.message = subl[options.message];
	}
	var res = {
		code: code,
		message: options.message
	};
	if(options.res){
		options.res.set('catch', '1');
		options.res.send(res, code);
	}
	delete options.res;
	delete options.req;
	app.c[logger]("%d - %j", code, options);
	return res;
}