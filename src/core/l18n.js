var fs = require('fs');
module.exports = function(path){
	var sub = {
		getSubtitle: function(symbol){
			var sub, pais = symbol.substring(0, symbol.indexOf('-'));
			sub = this[symbol] || this[pais];
			if(sub){
				return sub;
			}else{
				this['en'];
			}
		},
		getSubtitleByReq: function(req){
			var symbol = req.headers['accept-language'];
			symbol = symbol.substring(0, symbol.indexOf(';'));
			var sub, pais = symbol.substring(0, symbol.indexOf('-'));
			sub = this[symbol] || this[pais];
			if(sub){
				return sub;
			}else{
				this['en'];
			}
		}
	};
	fs.readdirSync(path).forEach(function (file) {
		var fileName = file.substring(0, file.length - 3);
    if(file.substr(-3) === '.js') {
      var subtitle = require(path + '/' + file);
      sub[fileName] = subtitle;
    }
  });
  return sub;
}