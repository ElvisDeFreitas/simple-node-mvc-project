module.exports = function(app){
	return {
		'getUserByEmail': function(email, callback){
			process.db.queryBulk(function(){
				var user = this.query(
					'SELECT u.id, u.name, u.email, u.password FROM user u WHERE u.email = ?;', [email]
				);
				if(user.length !== 1){
					callback('Usuário não encontrado');
					return ;
				}else{
					user = user[0];
				}
				user.roles = this.query("\
					SELECT ur.role_id as roleId, r.role FROM user_role ur \n\
					INNER JOIN role r ON r.id = ur.role_id \n\
					WHERE ur.user_id = ? \n\
				", [user.id]);

				user.rolesMap = (function(){
	        var roles = {};
	        this.roles.map(function(role){
	          roles[role.role] = true;
	        });
	        return roles;
		    }).call(user);

				callback(null, user);

			}, callback);
		},

	};
}