module.exports = function(app) {
	return {
		getSolution: function(callback){
			app.db.query('SELECT 1 + 1 AS solution', callback);
		},
		concurrenceTest: function(theName, callback){
			var connection = app.db;
			console.log("era....", connection.beginTransaction,connection);
			connection.beginTransaction(function(err) {
			  if (err) { throw err; }
			  
			  console.log("indo cadastrar...");
			  connection.query('INSERT INTO user (name) VALUES (?)', theName, function(err, result) {
			    console.log("cadastrou...", result.insertId);
			    if (err) {
			    	console.log("falhou na inserção do usuário");
			      return connection.rollback();
			    }

			    // var log = 'Post ' + result.insertId + ' added';
			    app.db.query('SELECT MAX(id) AS id FROM user', function(err, rows){
			    	console.log("pegando o ultimo...", err,rows);

			    		connection.query('INSERT INTO user SET name=?, ultimoId=?', [theName, rows[0].id], function(err, result) {
					      if (err) {
					      	console.log("erro ao inserir outro")
					        return connection.rollback(function() {
					          throw err;
					        });
					      }  
					      connection.commit(function(err) {
					        if (err) {
					          return connection.rollback(function() {
					            throw err;
					          });
					        }
					        callback(err);
					      });
					    });

			    });

			    
			  });
			});
			// app.db.query('SELECT 1 + 1 AS solution', callback);
		}
	}
};
