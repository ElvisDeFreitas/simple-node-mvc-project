var fs = require('fs');
    app = require("./src/core/Setup")();


// dynamically include routes (Controller)
try{
  fs.readdirSync('./src/controller').forEach(function (file) {
    if(file.substr(-3) === '.js') {
      process.c.debug("loading: %s", file);
      route = require('./src/controller/' + file);
      route.controller(app);
    }
  });
}catch(e){
  process.c.error("erro fatal sistema: ", e.toString());
}
app.listen(app.get('port'), function(){
  process.c.info('Express server listening on port ' + app.get('port'));
});
